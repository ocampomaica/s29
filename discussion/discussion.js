
db.inventory.insertMany([
		{
			"name": "Javascript for Beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50,
			"publisher": "JS Publishing House"
		},
		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38,
			"publisher": "NY Publishers"
		},
		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10,
			"publisher": "Big Idea Publishing"
		},
		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 10000,
			"stocks": 100,
			"publisher": "JS Publishing House"
		}
	]);

//Comparison Query Operator
/*
$gt operator - matches the values that are greater than a specified value

$gte operator - matches the values that are greater than or equal to a specified value

Syntax:
	$gt
	- db.collectionName.find({"field":{$gt: value}})

	$gte
	- db.collectionName.find({"field":{$gt: value}})
*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
})

db.inventory.find({
	"stocks": {
		$gte: 50
	}
})

/*
$lt operator - matches the values that are less than a specified value

$lte operator - matches the values that are less than or equal to a specified value

Syntax:
	$lt
	- db.collectionName.find({"field":{$lt: value}})

	$lte
	- db.collectionName.find({"field":{$lt: value}})
*/

db.inventory.find({
	"stocks": {
		$lt: 50
	}
})

db.inventory.find({
	"stocks": {
		$lte: 50
	}
})

/*
$ne (not equal) operator - matches the all values that are not equal to a specified value

Syntax:
	$ne
	- db.collectionName.find({"field":{$ne: value}})
*/

db.inventory.find({
	"stocks": {
		$ne: 50
	}
})

/*
$eq  - matches the all values that are equal to a specified value

Syntax:
	$eq
	- db.collectionName.find({"field":{$eq: value}})
*/

db.inventory.find({
	"stocks": {
		$eq: 50
	}
})

/*
$in  - matches any of the values specified in an array

Syntax:
	$eq
	- db.collectionName.find({"field":{$in: [value1, value2]}})
*/

db.inventory.find({
	"price": {
		$in: [5000, 10000]
	}
})

/*
$nin - matches none of the values specified in an array

Syntax:
	$eq
	- db.collectionName.find({"field":{$nin: [value1, value2]}})
*/

db.inventory.find({
	"price": {
		$nin: [5000, 10000]
	}
})

// To create a range condition
db.inventory.find({
	"price": {$gt: 2000} && {$lt: 4000}
})

/*
	Mini-Activity:
		1. In the inventory collection, return all the documents that have the price equal or less than 4000.
		2. In the inventory collection, return all the documents that have stocks of 50 and 100

*/

db.inventory.find({
	"price": {
		$lte: 4000
	}
})

db.inventory.find({
	"stocks": {
		$in: [50, 100]
	}
})

/*
Logical Query Operators

$or operator - Joins query clauses with a logical OR operator and returns all documents that match the conditions of their clauses

Syntax:
	db.collectionName.find({
		$or: [
			{"fieldA": "valueA"},
			{"fieldB": "valueB"}
		]
	})
*/

db.inventory.find({
	$or: [
		{"name": "HTML and CSS"},
		{"publisher": "JS Publishing House"}
	]
})

db.inventory.find({
	$or: [
		{"author": "James Doe"},
		{"price": {
				$lte: 5000
			}
		}
	]
})

/*
$and operator - Joins the query clauses with a logical AND operator and return all documents that match the conditions of both clauses.

Syntax:
	db.collectionName.find({
		$and: [
			{"fieldA": "valueA"},
			{"fieldB": "valueB"}
		]
	})
*/

db.inventory.find({
	$and: [
		{"stocks": {
			$ne: 50
			}
		},
		{"price": {
				$ne: 5000
			}
		}
	]
})

//Without the $and operator
db.inventory.find({
	"stocks": {
		$ne: 50
	},
	"price": {
		$ne: 5000
	}
})

// Field Projection

/*
Inclusion - matches the document according to the given criteria and returns included field/s

Syntax:
	db.collectionName.find({criteria}, {field: 1})
*/

db.inventory.find({
	"publisher": "JS Publishing House"},
	{
		"name": 1,
		"author": 1,
		"price": 1
	}
	)

/*
Exclusion - matches document/s with the given criteria and returns the fields that were not excluded.

Syntax:
	db.collectionName.find({criteria}, {field: 1})
*/

db.inventory.find({
	"author": "Noah Jimenez"},
	{
		"price": 0,
		"stocks": 0
	}
	)

// We can't combine inclusion and exclusion except when hiding the _id

db.inventory.find(
{
	"price": {
		$lte: 5000}
	},
	{
		"_id": 0,
		"name": 1,
		"author": 1
	}
)

/*
EVALUATION QUERY OPERATOR

$regex operator (regular expressions) - selects documents where values match a specified regular expression

Syntax:
	db.collectionName.find({"field": {$regex: 'pattern'}})
*/
// case sensitive
db.inventory.find({
	"author": {
		$regex: 'M'
	}
})

// case insensitive
db.inventory.find({
	"author": {
		$regex: 'M'
		$option: '$i'
	}
})

/*
	Mini Activity:
		- in the inventory collections, find a "name" of a book/s with letters "java" in it and has a "price" of greater than or equal to 5000.
			-- use $and, $regex and $gte operators
*/

db.inventory.find({
    $and: [
    {
    	"name": {
    		$regex: 'java', $options: '$i'
    	}
    },
    {
    	"price": {
    		$gte: 5000
    	}
    } ]
})
















